package com.example.jetpackagedemo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class DetailsViewModel(val number: Int) : ViewModel() {
    // TODO: Implement the ViewModel
}

class  DetailsViewModelFactory(private val number: Int) : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return DetailsViewModel(number) as T
    }
}


fun Injector.provideDetailsViewModel(number: Int): DetailsViewModelFactory = DetailsViewModelFactory(number)

