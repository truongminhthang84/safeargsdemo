package com.example.jetpackagedemo

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class HomeViewModel : ViewModel() {
    // TODO: Implement the ViewModel

}

class  HomeViewModelFactory : ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return HomeViewModel() as T
    }
}



fun Injector.provideHomeViewModel(): HomeViewModelFactory = HomeViewModelFactory()